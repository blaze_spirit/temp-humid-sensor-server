const express     = require('express');
const bodyParser  = require('body-parser');
const http        = require('http');
const Influx      = require('influx');
const json2csv    = require('json2csv');
const utils       = require('./utils.js');
const config      = require('./config.js');
const DB_NAME     = config.dbName;
const MEASUREMENT = config.measurement;

const server = express();

const influx = new Influx.InfluxDB({
    host: 'localhost',
    database: DB_NAME,
    schema: [
        {
            measurement: MEASUREMENT,
            fields: {
                temp: Influx.FieldType.FLOAT,
                humid: Influx.FieldType.FLOAT
            },
            tags: [
                'alias_name',
                'sensor_name',
                'desc'
            ]
        }
    ]
});

// serve html and other static files
server.use(express.static('public'));

// get data from POST method
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// enable CORS & cache-control
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Cache-Control', 'no-cache');
    next();
});

// serve dashboard
server.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// get sensor list
server.get('/sensor-list', function(req, res) {
    influx.query(
        `
        SELECT * from sensor_info
        GROUP BY alias_name
        ORDER BY time DESC
        limit 1
        `
    )
    .then(result => {
        res.json(result);
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error getting sensor list! ${err.stack}`);
    });
});

// get latest sensor info reading for dashboard
server.get('/sensor-reading', function(req, res) {
    influx.query(
        `
        SELECT *
        FROM temp_humid
        GROUP BY "alias_name"
        ORDER BY time DESC
        LIMIT 1
        `
    )
    .then(result => {
        res.json(result.groups());
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error getting temperature! ${err.stack}`);
    });
});

// update sensor info
server.post('/sensor-info', function(req, res) {
    let sensorData = {
        measurement: 'sensor_info',
        fields: {
            name: req.body.name,
            desc: req.body.desc
        },
        tags: {
            alias_name: req.body.aliasName,
        }
    };

    influx
    .writePoints([sensorData])
    .then(() => {
        res.status(200).send('OK');
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error updating sensor info! ${err.stack}`);
    });
});

// endpoint for Billion gateway to post sensor data
server.post('/write-sensor-data', function(req, res) {
    let sensorReadingArray = [];

    if (Array.isArray(req.body)) {
        let readingArray = req.body;

        readingArray.forEach(reading => {
            let timestamp = new Date(parseInt(reading.time) * 1000);
            let devices   = reading.devices;

            devices.forEach(device => {
                let point = {
                    measurement: config.measurement,
                    timestamp: timestamp,
                    fields: {
                        temp: parseFloat(device.temperature),
                        humid: parseFloat(device.humidity)
                    },
                    tags: {
                        alias_name: device.alias
                    }
                };
                sensorReadingArray.push(point);
            });
        })
    }
    else {
        let responseData = req.body;
        let timestamp = new Date(parseInt(responseData.time) * 1000);
        let devices = responseData.devices;

        devices.forEach(device => {
            let point = {
                measurement: config.measurement,
                timestamp: timestamp,
                fields: {
                    temp: parseFloat(device.temperature),
                    humid: parseFloat(device.humidity)
                },
                tags: {
                    alias_name: device.alias
                }
            };
            sensorReadingArray.push(point);
        });
    }

    influx
    .writePoints(sensorReadingArray)
    .then(() => {
        res.status(200).send('OK');
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error writting sensor reading to ${config.dbName}! ${err.stack}`);
    });
});

// get temperature
server.get('/temp/:start/:end/:group', function (req, res) {
    let start = req.params.start;
    let end   = req.params.end;
    let group = req.params.group;

    influx.query(
        `
        SELECT * FROM (
            SELECT MEAN(temp) AS "temp"
            FROM temp_humid
            WHERE time >= ${start}ms
            AND time < ${end}ms
            GROUP BY time(${group}ms)
            FILL(-100)
        ) GROUP BY alias_name
        `
    )
    .then(result => {
        res.json(result.groups());
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error getting temperature! ${err.stack}`);
    });
});

// get humidity
server.get('/humid/:start/:end/:group', function (req, res) {
    let start = req.params.start;
    let end   = req.params.end;
    let group = req.params.group;

    influx.query(
        `
        SELECT * FROM (
            SELECT MEAN(humid) AS "humid"
            FROM temp_humid
            WHERE time >= ${start}ms
            AND time < ${end}ms
            GROUP BY time(${group}ms)
            FILL(-100)
        ) GROUP BY alias_name
        `
    )
    .then(result => {
        res.json(result.groups());
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error getting humidity! ${err.stack}`);
    });
});

// check if that particular month have data to export
// checking range is from now up to 6 months ago
// return an array of timestamp (in ms at start of the month)
// indicate which month have data (eg. [1514764800000, 1517443200000, ..])
// * use by front end to generate export dropdown menu
server.get('/get-available-export-period', function (req, res) {
    let promiseArray = [];

    for (let i = 0; i < 6; i++) {
        let queryDateObjStart = new Date();
        let queryDateObjEnd   = new Date();

        queryDateObjStart.setMonth(queryDateObjStart.getMonth() - i, 1);
        queryDateObjStart.setHours(0, 0, 0, 0);

        queryDateObjEnd.setMonth(queryDateObjEnd.getMonth() - i + 1, 1);
        queryDateObjEnd.setHours(0, 0, 0, 0);

        promiseArray.push(influx.query(
            `
            SELECT COUNT(temp) AS "record"
            FROM temp_humid
            WHERE time >= ${queryDateObjStart.getTime()}ms
            AND time < ${queryDateObjEnd.getTime()}ms
            `
        ));
    }

    Promise
    .all(promiseArray)
    .then(resultArray => {
        let resultList = [];

        resultArray.forEach(result => {
            if (result[0] && result[0].time) {
                resultList.push({
                    timestamp: result[0].time.getTime(),
                    record: result[0].record
                });
            }
        });
        res.json(resultList);
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error getting available export period! ${err.stack}`);
    });
});

// get export sensor data
server.get('/export-data/:start/:end', function (req, res) {
    let start = req.params.start;
    let end   = req.params.end;

    influx.query(
        `
        SELECT *
        FROM temp_humid
        WHERE time >= ${start}ms
        AND time < ${end}ms
        GROUP BY alias_name
        `
    )
    .then(result => {
        let groupedResult = result.groups();

        let fields = ['timestamp', 'aliasName', 'temp', 'humid'];
        let data = [];

        groupedResult.forEach(series => {
            series.rows.forEach(dataSet => {
                data.push({
                    timestamp: new Date(dataSet.time).toLocaleString(),
                    aliasName: dataSet.alias_name,
                    temp: dataSet.temp,
                    humid: dataSet.humid
                });
            });
        });

        let csv = json2csv({ data: data, fields: fields });

        // generate file name
        let dateObj = new Date(parseInt(start));
        let year = dateObj.getFullYear();
        let month = dateObj.getMonth();

        switch (month) {
            case 0: month = 'January'; break;
            case 1: month = 'February'; break;
            case 2: month = 'March'; break;
            case 3: month = 'April'; break;
            case 4: month = 'May'; break;
            case 5: month = 'June'; break;
            case 6: month = 'July'; break;
            case 7: month = 'August'; break;
            case 8: month = 'September'; break;
            case 9: month = 'October'; break;
            case 10: month = 'November'; break;
            case 11: month = 'December'; break;
        }

        res.attachment(month + '_' + year + '_temp_humid' + '.csv');
        res.status(200).send(csv);
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error getting export data! ${err.stack}`);
    });
});

// init influxDB & start the server
influx.getDatabaseNames()
    .then(dbNames => {
        if(!dbNames.includes(DB_NAME)) {
            return influx.createDatabase(DB_NAME);
        }
    })
    .then(() => {
        // check if DB is initialized with default sensor data
        return utils.isInfluxInitialized(influx);
    })
    .then(result => {
        if (result === 0) {
            utils.insertDefaultSensorInfoToDB(influx);
        }

        http.createServer(server).listen(3000, () => {
            console.log('Listening on port 3000');
        });
    })
    .catch(err => {
        console.error(`Error: ${err}`);
    });

