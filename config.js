const DB_NAME                 = 'test_db';
const MEASUREMENT             = 'temp_humid';
const GATEWAY_SENSOR_ENDPOINT = 'http://192.168.5.254/api_json.asp?cmd=showall&auth=YWRtaW46YWRtaW4=';
const DEFAULT_SENSOR_LIST     = [
    {
        aliasName: 'sensor_01',
        name: 'sensor 1',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_02',
        name: 'sensor 2',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_03',
        name: 'sensor 3',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_04',
        name: 'sensor 4',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_05',
        name: 'sensor 5',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_06',
        name: 'sensor 6',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_07',
        name: 'sensor 7',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_08',
        name: 'sensor 8',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_09',
        name: 'sensor 9',
        desc: 'no description'
    },
    {
        aliasName: 'sensor_10',
        name: 'sensor 10',
        desc: 'no description'
    }
];

module.exports = {
    dbName: DB_NAME,
    measurement: MEASUREMENT,
    gatewaySensorEndpoint: GATEWAY_SENSOR_ENDPOINT,
    defaultSensorList: DEFAULT_SENSOR_LIST
};