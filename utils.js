const toNanoDate = require('influx').toNanoDate;
const config     = require('./config.js');

function isInfluxInitialized(influx) {
    return new Promise((resolve, reject) => {
        influx.query(
            `
            SELECT COUNT(*) FROM sensor_info
            
            `
        )
        .then(result => {
            resolve(result.length);
        })
        .catch(err => {
            reject(err);
        });
    });   
}

function insertDefaultSensorInfoToDB(influx) {
    console.log('Init influxDB with default sensor info');

    let sensorInfoList = [];

    config.defaultSensorList.forEach(sensor => {
        sensorInfoList.push({
            measurement: 'sensor_info',
            fields: {
                name: sensor.name,
                desc: sensor.desc
            },
            tags: {
                alias_name: sensor.aliasName
            }
        });
    });

    influx
    .writePoints(sensorInfoList)
    .catch(err => {
        console.error(`Error writting sensor_info to ${config.dbName}! ${err.stack}`);
    });
}

module.exports = {
    isInfluxInitialized,
    insertDefaultSensorInfoToDB
};