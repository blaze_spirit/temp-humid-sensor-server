const toNanoDate  = require('influx').toNanoDate;
const config      = require('./config.js');
const MEASUREMENT = config.measurement;

async function pumpDataToDB(influx) {
    let recordInserted = 0;

    let startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
    let endDate   = new Date(2019, 0, 1, 0, 0, 0, 0);
    let currentDate = new Date(startDate.getTime());

    while (currentDate.getTime() < endDate.getTime()) {
        // generate data points in batch of 2000.
        let batchData = [];
        
        for(let i = 0; i < 2000; i++) {
            let currentDateNano = toNanoDate(currentDate.getTime() + '000000');

            let dataPointArray = [
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_01',
                        sensor_name: 'sensor 1',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_02',
                        sensor_name: 'sensor 2',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_03',
                        sensor_name: 'sensor 3',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_04',
                        sensor_name: 'sensor 4',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_06',
                        sensor_name: 'sensor 6',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_05',
                        sensor_name: 'sensor 5',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_07',
                        sensor_name: 'sensor 7',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_08',
                        sensor_name: 'sensor 8',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_09',
                        sensor_name: 'sensor 9',
                        desc: 'no description'
                    }
                },
                {
                    measurement: MEASUREMENT,
                    timestamp: currentDateNano,
                    fields: {
                        temp: _generateRandomTemp(),
                        humid: _generateRandomHumid()
                    },
                    tags: {
                        alias_name: 'sensor_10',
                        sensor_name: 'sensor 10',
                        desc: 'no description'
                    }
                },
            ]

            dataPointArray.forEach(dataPoint => {
                batchData.push(dataPoint);
            });

            // adjust currentDate by adding 1 minute.
            currentDate = new Date(currentDate.getTime() + 60000);
            recordInserted += 10;
        }

        await influx.writePoints(batchData)
            .catch(err => {
                console.error(`Error saving data to InfluxDB! ${err.stack}`);
            });

        if (recordInserted === 2000000) {
            console.log('20% completed');
        }
        else if (recordInserted === 4000000) {
            console.log('40% completed');
        }
        else if (recordInserted === 6000000) {
            console.log('60% completed');
        }
        else if (recordInserted === 8000000) {
            console.log('80% completed');
        }
        else if (recordInserted === 10000000) {
            console.log('95% completed');
        }
    }

    console.log(`Records inserted: ${recordInserted}`);
}

function _generateRandomTemp() {
    // get random number between 20 to 30
    randomTemp = Math.random() * (30 - 20) + 20;

    // round to 2 decimal place
    randomTemp = parseFloat(randomTemp.toFixed(2));

    return randomTemp;
}

function _generateRandomHumid() {
    // get random number between 0 to 100
    randomHumid = Math.random() * 100;

    // round to integer
    randomHumid = Math.round(randomHumid);

    return randomHumid;
}

module.exports = {
    pumpDataToDB
};