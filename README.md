# Overview #

Temperature & humidity sensor server.


### Set up ###

1. clone the repo.
2. run command 'npm install'.
3. start the influxDB.
4. run the server using command 'node main.js'. The influxDB must be running before server start.
5. refer to the config file for configurations.

### Requirement ###

* InfluxDB v1.4
* NodeJS v8.9.1
* NPM v5.6.0